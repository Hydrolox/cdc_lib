#include <cdc_lib/file/archive_fs.h>
#include <cstdio>
#include <cstdlib>
#include <cxxopts.hpp>
#include <fmt/core.h>

std::string guess_record_type( const cdc_lib::file::archive& a_archive, const cdc_lib::file::archive_record& a_record )
{
    const auto data = cdc_lib::file::read_record( a_archive, a_record );
    if( data.empty() )
        return "empty file";
    if( data[0] == '\x16' )
        return "drm";
    constexpr auto k_cine_magic_pos = 0x2010;
    if( data.size() >= (k_cine_magic_pos + sizeof( std::uint32_t )) &&
        data[k_cine_magic_pos] == 'E' &&
        data[k_cine_magic_pos + 1] == 'N' &&
        data[k_cine_magic_pos + 2] == 'I' &&
        data[k_cine_magic_pos + 3] == 'C' )
        return "cine";
    if( data.size() >= sizeof( std::uint32_t ) &&
        data[0] == '\x44' &&
        data[1] == '\xAC' )
        return "mul";
    return "unknown";
}

int main( int argc, char** argv )
{
    auto options = cxxopts::Options{"cdc_lib_archive_dump", "Dump target archive"};
    options.add_options()
        ( "file", "The archive to dump", cxxopts::value< std::string >() )
        ( "s,sort-by",
          "Sort by this field (valid: name_hash, offset, spec_mask, size)",
          cxxopts::value< std::string >()->default_value( "name_hash" ) );
    options.parse_positional( {"file"} );
    options.positional_help( "<file>" );
    auto result = options.parse( argc, argv );
    if( argc < 2 )
    {
        fmt::print( stderr, "{}\n", options.help() );
        return EXIT_FAILURE;
    }
    const auto filename = result["file"].as< std::string >();
    auto i_interface = score::binary_io::open_file( filename );
    auto archive = cdc_lib::file::load_tiger_archive( *i_interface, filename );
    fmt::print( "archive \"{}\":\n", filename );
    fmt::print( " count: {}\n", archive.archive_count );
    fmt::print( " dlc: {}\n", archive.dlc_index );
    fmt::print( " num records: {}\n", archive.records.size() );
    fmt::print( " config: {}\n", archive.config_name );
    std::vector records = archive.records;
    std::ranges::sort(  records,
                        [&result]( const auto& a, const auto& b ) -> bool
                        {
                            const auto sort_by = result["sort-by"].as< std::string >();
                            if( sort_by == "name_hash" )
                                return a.name_hash < b.name_hash;
                            if( sort_by == "offset" )
                                return a.offset < b.offset;
                            if( sort_by == "spec_mask" )
                                return a.spec_mask < b.spec_mask;
                            if( sort_by == "size" )
                                return a.size < b.size;
                            return false;
                        } );
    for( const auto& record : records )
    {
        const auto type = guess_record_type( archive, record );
        fmt::print( "record {:08x}, spec {:08x}, {:8} bytes, at {:08x}, type {}\n",
                    record.name_hash,
                    record.spec_mask,
                    record.size,
                    record.offset,
                    type );
    }
    return EXIT_SUCCESS;
}
